//
//  StartVC.swift
//  VideoApp
//
//  Created by archie on 05/03/21.
//

import UIKit

class StartVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func audioCall_action(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AudioVideoVC") as! AudioVideoVC
        vc.videoEnabled = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func videoCall_action(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AudioVideoVC") as! AudioVideoVC
        vc.videoEnabled = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
