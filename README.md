# AudioVideoCalling

This is a basic audio/video calling application.

The app needs to be launched simultaneously on two devices. 

**Audio Calling**
1. From the initial screen, click Audio Call button on both the devices. 
2. The session will start and the two devices will be automatically connected.
3. The call can be disconnected using the Disconnect button at the top.

**Video Calling**
1. From the initial screen, click Video Call button on both the devices. 
2. The session will start and the two devices will be automatically connected.
3. The users can see live video streaming on their screens.
4. The call can be disconnected using the Disconnect button at the top.
